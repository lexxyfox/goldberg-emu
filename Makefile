# https://stackoverflow.com/a/25364355
# https://ismail.badawi.io/blog/automatic-directory-creation-in-make

CXXFLAGS += -flto=auto -fPIC -O3 -pipe -s -std=c++14 -DNDEBUG -DEMU_RELEASE_BUILD
LDFLAGS += -shared -lprotobuf-lite -ldl

SRCDIR ?= dll
OBJDIR ?= obj

libsteam_api.so: $(addprefix $(OBJDIR)/, $(addsuffix .o, \
		base dll flat local_storage net.pb network settings settings_parser wrap \
		steam_applist steam_apps steam_client steam_gameserver steam_gameserverstats steam_http \
		steam_matchmaking_servers steam_music steam_musicremote steam_parental steam_screenshots steam_video \
	))
	+$(CXX) $(CXXFLAGS) -o '$@' $^ $(LDFLAGS)
	
$(SRCDIR)/net.pb.cpp $(SRCDIR)/net.pb.h: $(SRCDIR)/net.proto
	cd $(SRCDIR) && protoc net.proto --cpp_out .
	mv $(SRCDIR)/net.pb.cc $(SRCDIR)/net.pb.cpp

clean:
	$(RM) -r libsteam_api.so $(OBJDIR) $(SRCDIR)/net.pb.cc $(SRCDIR)/net.pb.h

.PRECIOUS: %/

%/:
	mkdir -p $@

.SECONDEXPANSION:

$(OBJDIR)/%.o : $(SRCDIR)/%.cpp | $(SRCDIR)/net.pb.h $$(@D)/
	+$(CXX) $(CXXFLAGS) -o '$@' -c '$<'
