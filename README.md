# OwO wats dis??

This is my personal implementation of Mr. Goldberg's emulator. It works great for me, my friends, and our games. No support will be provided beyond what's written here, sorry! Please check out the original implementation for general use cases and additional support.

# Environment Variables

| name | desc | 
| ---- | ---- |
| SteamAppId |      | 
| SteamAccountName | |
